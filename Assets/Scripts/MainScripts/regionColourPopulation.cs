﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class regionColourPopulation : MonoBehaviour {
	
	public Slider populationSlider;
	public float maxPopulation;
	private float currentPopulation;
	
	public GameObject[] regionsColourChange;
	
	public float smooth = 2;
	
	private Color newColourMotherRussia;
	private Color newColourMiddleEast;
	private Color newColourEurope;
	private Color newColourAfrica;
	private Color newColourSouthAmerica;
	private Color newColourUSA;
	private Color newColourAustralia;
	
	private Color green = new Color (0f, 0.7f, 0f, 1f);
	private Color yellow = new Color (1f, 0.92f, 0.016f, 1f);
	private Color red = new Color (1f, 0f, 0f, 1f);
	
	
	private float[] population = {100,100,100,100,100,100,100};
	
	void Awake(){
		
		currentPopulation = maxPopulation;
		
		regionsColourChange = GameObject.FindGameObjectsWithTag ("regions");
		
		
		for(int i = 0; i < regionsColourChange.Length; i++)
		{
			regionsColourChange[i].GetComponent<Image>().color = green;
			
			newColourMotherRussia = regionsColourChange [0].GetComponent<Image> ().color;
			newColourMiddleEast = regionsColourChange [1].GetComponent<Image> ().color;
			newColourEurope = regionsColourChange [2].GetComponent<Image> ().color;
			newColourAfrica = regionsColourChange [3].GetComponent<Image> ().color;
			newColourSouthAmerica = regionsColourChange [4].GetComponent<Image> ().color;
			newColourUSA = regionsColourChange [5].GetComponent<Image> ().color;
			newColourAustralia = regionsColourChange [6].GetComponent<Image> ().color;
		}
		
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		//******************************************
		//These two if statements are just temporary until we have something to manipulate the population.
		//Just use WASD or arrow keys. Vertical being up/down, horizontal being left/right.
		//if(Input.GetButton ("Vertical"))
		//{
		populationDeath();
		//}
		
		
		//if(Input.GetButton ("Horizontal"))
		//{
		populationBirth();
		//}
		
		
		
		
		
		//********************************************
		
		colourChanging ();
		
		//just temporary until we figure out what to do with population numbers
		if(Time.time > 5f && Time.time < 20f)
		{
			population[0] = population[0] - 0.1f;
			population[1] = population[1] - 0.2f;
			population[2] = population[2] - 0.3f;
			population[3] = population[3] - 0.07f;
			population[4] = population[4] - 0.25f;
			population[5] = population[5] - 0.15f;
			population[6] = population[6] - 0.08f;
			//print ("Russia " + population[0]);
			//print ("South America " + population[4]);
		}
		
		//just temporary until we figure out what to do with population numbers
		if(Time.time > 20f)
		{
			population[0] = population[0] + 0.1f;
			population[1] = population[1] + 0.2f;
			population[2] = population[2] + 0.3f;
			population[3] = population[3] + 0.07f;
			population[4] = population[4] + 0.25f;
			population[5] = population[5] + 0.15f;
			population[6] = population[6] + 0.08f;
			//print ("Russia " + population[0]);
			//print ("South America " + population[4]);
		}
		
	}
	
	
	
	//decreases population
	void populationDeath()
	{
		
		if (currentPopulation <= 0f) 
		{
			return;
		}
		
		//Change below values as it is just an example
		if (population [0] < 100f && population[0] >= 20f) 
		{
			currentPopulation = currentPopulation -0.03f;
			populationSlider.value = currentPopulation;
		}
		
	}
	
	//adds population
	void populationBirth()
	{
		
		if (currentPopulation >= 100f) 
		{
			return;
		}
		
		//Change below values as it is just an example
		if (population[0] < 20f)
		{
			currentPopulation = currentPopulation +0.05f;
			populationSlider.value = currentPopulation;
		}
	}
	
	
	
	
	
	//Changes colour of region depending on population
	public void colourChanging()
	{
		//***********************MotherRussia********************
		if(population[0] > 80f)
		{
			newColourMotherRussia = green;
		}
		
		if(population[0] <= 80f && population[0] > 40f)
		{
			newColourMotherRussia = yellow;
		}
		
		if(population[0] <= 40f)
		{
			newColourMotherRussia = red;
		}
		//***********************MotherRussia********************
		
		//+++++++++++++++++++++++MiddleEast++++++++++++++++++++++
		if(population[1] > 80f)
		{
			newColourMiddleEast = green;
		}
		
		if(population[1] <= 80f && population[1] > 40f)
		{
			newColourMiddleEast = yellow;
		}
		
		if(population[1] <= 40f)
		{
			newColourMiddleEast = red;
		}
		//+++++++++++++++++++++++MiddleEast++++++++++++++++++++++
		
		//***********************Europe**************************
		if(population[2] > 80f)
		{
			newColourEurope = green;
		}
		
		if(population[2] <= 80f && population[2] > 40f)
		{
			newColourEurope = yellow;
		}
		
		if(population[2] <= 40f)
		{
			newColourEurope = red;
		}
		//***********************Europe**************************
		
		//+++++++++++++++++++++++Africa++++++++++++++++++++++++++
		if(population[3] > 80f)
		{
			newColourAfrica = green;
		}
		
		if(population[3] <= 80f && population[3] > 40f)
		{
			newColourAfrica = yellow;
		}
		
		if(population[3] <= 40f)
		{
			newColourAfrica = red;
		}
		//+++++++++++++++++++++++Africa++++++++++++++++++++++++++
		
		//***********************SouthAmerica********************
		if(population[4] > 80f)
		{
			newColourSouthAmerica = green;
		}
		
		if(population[4] <= 80f && population[4] > 40f)
		{
			newColourSouthAmerica = yellow;
		}
		
		if(population[4] <= 40f)
		{
			newColourSouthAmerica = red;
		}
		//***********************SouthAmerica********************
		
		//+++++++++++++++++++++++USA+++++++++++++++++++++++++++++
		if(population[5] > 80f)
		{
			newColourUSA = green;
		}
		
		if(population[5] <= 80f && population[5] > 40f)
		{
			newColourUSA = yellow;
		}
		
		if(population[5] <= 40f)
		{
			newColourUSA = red;
		}
		//++++++++++++++++++++++++USA+++++++++++++++++++++++++++++
		
		//***********************Ausralia*************************
		if(population[6] > 80f)
		{
			newColourAustralia = green;
		}
		
		if(population[6] <= 80f && population[6] > 40f)
		{
			newColourAustralia = yellow;
		}
		
		if(population[6] <= 40f)
		{
			newColourAustralia = red;
		}
		//************************Ausralia*************************
		
		
		
		//gradually changes from one colour to another.
		regionsColourChange [0].GetComponent<Image> ().color = Color.Lerp (regionsColourChange [0].GetComponent<Image> ().color, newColourMotherRussia, Time.deltaTime * smooth);
		regionsColourChange [1].GetComponent<Image> ().color = Color.Lerp (regionsColourChange [1].GetComponent<Image> ().color, newColourMiddleEast, Time.deltaTime * smooth);
		regionsColourChange [2].GetComponent<Image> ().color = Color.Lerp (regionsColourChange [2].GetComponent<Image> ().color, newColourEurope, Time.deltaTime * smooth);
		regionsColourChange [3].GetComponent<Image> ().color = Color.Lerp (regionsColourChange [3].GetComponent<Image> ().color, newColourAfrica, Time.deltaTime * smooth);
		regionsColourChange [4].GetComponent<Image> ().color = Color.Lerp (regionsColourChange [4].GetComponent<Image> ().color, newColourSouthAmerica, Time.deltaTime * smooth);
		regionsColourChange [5].GetComponent<Image> ().color = Color.Lerp (regionsColourChange [5].GetComponent<Image> ().color, newColourUSA, Time.deltaTime * smooth);
		regionsColourChange [6].GetComponent<Image> ().color = Color.Lerp (regionsColourChange [6].GetComponent<Image> ().color, newColourAustralia, Time.deltaTime * smooth);
		
	}
}
