using UnityEngine;
using System.Collections;


public class PlaneTrajectory : MonoBehaviour 
{
	LineRenderer line;
	// Use this for initialization
	void Start ()
	{

		line = gameObject.GetComponent<LineRenderer> ();
		line.enabled = true;
		print (line.isVisible);

		Vector3 a = new Vector3(-121.64f, 38.96f, -1.0f);
		Vector3 b = new Vector3(-21.62f, 32.86f, -1.0f);

	
		plotArc (a,b);

	}
	
	// Update is called once per frame
	void Update ()
	{

	}


	/* Returns quadractic coordinates after bezier curve is worked out */
	/* t has to be between 0 and 1 */

	Vector3 GetQuadraticCoordinates(float t, Vector3 startPoint, Vector3 endPoint) 
	{
		/* Middle Point affects the arcness of the line */
		Vector3 middlePoint =  (startPoint - endPoint );

		/*f to the power of p (i.e 1-t to the power of 2*/
		/* gets quadratric coordinate for this trajectory */
		Vector3 startResult = (Mathf.Pow (1 - t, 2) * startPoint);

		Vector3 midResult = (2 * t * (1 - t) * middlePoint);

		Vector3 endResult = (Mathf.Pow (t, 2) * endPoint);

		Vector3 result = (startResult + midResult) + endResult;
		result.z = -1.0f;

		return result;
	}


	/* plots trajectory arc for transmission vehicle i.e. plane */
	void plotArc(Vector3 a, Vector3 b)
	{
			
		float t;
		line.SetVertexCount(10);
		for (int i =0; i < 10; i++) 
		{	
			t = (float)i/(9.0f);

			line.SetPosition(i, GetQuadraticCoordinates(t, a, b));
		}
	}

}


