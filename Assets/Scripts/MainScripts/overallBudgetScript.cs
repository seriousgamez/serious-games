﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class overallBudgetScript : MonoBehaviour {

	public int budget;
	Text text;


	// Use this for initialization
	void Start () {

		text = GetComponent<Text>();
		budget = 1000;
	
	}
	
	// Update is called once per frame
	void Update () {

		text.text = "Budget: " + budget;
	
	}
}
