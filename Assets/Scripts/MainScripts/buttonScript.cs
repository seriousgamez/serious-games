﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class buttonScript : MonoBehaviour {


	public bool airportToggle = true;
	Animator anim;

	public overallBudgetScript budgetClass;
	public GameObject budgetTextObject;



	// Use this for initialization
	void Awake () {

		budgetClass = budgetTextObject.GetComponent<overallBudgetScript>();

		anim = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
	

	}


	public void changeAirportState()
	{

		if (airportToggle == false) 
		{

			
			anim.SetBool ("airportBool", !airportToggle); 
			
			airportToggle = true; 

			
		}else if (airportToggle == true) 
		{  
			
			anim.SetBool ("airportBool", !airportToggle);

			airportToggle = false;

			budgetClass.budget = budgetClass.budget - 100;

		}


	}



}
