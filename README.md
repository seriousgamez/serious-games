# README #

PLEASE NOTE:

Do not push The library folder and anything it contains.

How to use Git.

Getting the folder first time around


1) Go to your destination folder you want the project to be in

2) right click inside the folder and go to git clone ( see pic)

![stuff1.png](https://bitbucket.org/repo/bn5EBM/images/2406573639-stuff1.png)

3) Here the tortoiseGit UI will appear with URL and DIrectory: URL being the URL of this repository shown at the top right of the overview (https) and the directory being the path on your computer you want it stored

example URL (Mine for this repository is https://lmcvie@bitbucket.org/seriousgamez/serious-games.git) yours should be something like that.

![stuff2.png](https://bitbucket.org/repo/bn5EBM/images/3848865523-stuff2.png)


4) When you press okay it will start to transfer the data then ask you for your Bitbucket password (so you can access stuff stored on the website) it will then look like this if you did everything right

![stuff3.png](https://bitbucket.org/repo/bn5EBM/images/1681832652-stuff3.png)

you will then have the up to date project on your computer in that folder you picked.



After making changes to the project, you can then push your work onto the website so other people in the group can access it.



1) Commit your changes regularly, every time you do something that changes how the program works then you commit the change - this basically saves the changes to your computer a long with a message (such as "Added in new artwork for game") when you then put the update onto the website we will see this message.

![stuff4.png](https://bitbucket.org/repo/bn5EBM/images/3550319125-stuff4.png)

after pressing commit you will see this

where you enter the message and make sure to tick any new files you want to add to the project online


![stuff5.png](https://bitbucket.org/repo/bn5EBM/images/4192578679-stuff5.png)

once you have pressed save then you can push the project


![stuff6.png](https://bitbucket.org/repo/bn5EBM/images/1979186560-stuff6.png)

once you have pressed push make sure the top is on the master then make sure you have clicked the arbitrary url part then paste in your url for bitbucket again

![stuff7.png](https://bitbucket.org/repo/bn5EBM/images/2636493729-stuff7.png)

press okay after then it will ask you for your password for bitbucket again press that and you will have pushed to the website you can then check the commits section on the repository website to see your update.



Updating your version of the project before your work



1) at some stage someone else in the group will have made progress whilst you were not working and pushed it to the website. To make sure your working on the most updated version you need to "pull" updates from the website before beginning work.

![stuff8.png](https://bitbucket.org/repo/bn5EBM/images/3498167323-stuff8.png)

2) just make sure the url is correct for your bitbucket and the master and origin is used then press okay and enter your bitbucket password it will update your folder on your computer if successful and the folder should have a green tick on it (telling you its synced with the website)

Thats the basics of using git with bitbucket and if you need any other help just skype me or ask me 

Liam